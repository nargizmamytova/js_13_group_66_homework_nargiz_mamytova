import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllCountriesComponent } from './all-countries/all-countries.component';
import { CountriesDetailsComponent } from './countries-details/countries-details.component';
import { CountryResolverService } from './countries-details/country-resolver.service';
import { NotFounds } from './not-founds';

const routes: Routes = [
  {
    path: ':id',
    component: CountriesDetailsComponent,
    resolve: {
      country: CountryResolverService
    }
  },
  {path: '**', component: NotFounds}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
