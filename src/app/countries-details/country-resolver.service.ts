import { Injectable } from '@angular/core';
import { Country } from '../shared/country.model';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CountryService } from '../shared/country.service';

@Injectable({
  providedIn: 'root'
})
export class CountryResolverService implements Resolve<Country> {

  constructor( private countriesService: CountryService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Country>{
    const countryId = route.params['id'];
    return this.countriesService.fetchCountry(countryId)
  }
}
