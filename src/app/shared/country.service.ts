import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Country } from './country.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class CountryService{
  countriesChange = new Subject<Country[]>();
  countriesFetching = new Subject<boolean>();

  constructor(private http: HttpClient) {}
  private allCountries: Country[] = [];

  getCountries(){
    return this.allCountries.slice();
  }
  fetchCountry(countryId: string){
       return this.http.get<Country>
      (`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${countryId}`)
        .pipe(map(result => {
          return new Country(
            result.name,result.alpha3Code, result.capital,
            result.flag,
            result.population,
            result.subregion
          )
        }))
  }

  fetchCountries(){
    this.countriesFetching.next(true);
    this.http.get<{[id: string]: Country}>
    ('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const countriesData = result[id]
          return new Country (
            countriesData.name, countriesData.alpha3Code,
           )
         })
      }))
      .subscribe(countries => {
        this.allCountries = countries;
        this.countriesChange.next(this.allCountries.slice());
        this.countriesFetching.next(false)
      }, error => {
        this.countriesFetching.next(false)
      })
  }
}
