export class Country{
  constructor(
    public name?: string,
    public alpha3Code?: string,
    public capital?: string,
    public flag?: string,
    public population?: string,
    public subregion?: string,
  ) {}
}
