import { Component, OnDestroy, OnInit } from '@angular/core';
import { Country } from '../shared/country.model';
import { Subscription } from 'rxjs';
import { CountryService } from '../shared/country.service';

@Component({
  selector: 'app-all-countries',
  templateUrl: './all-countries.component.html',
  styleUrls: ['./all-countries.component.css']
})
export class AllCountriesComponent implements OnInit, OnDestroy {
  loading: boolean = false;
  allCountries!: Country[];
  countriesChangeSubscription!: Subscription;
  countriesFetchingSubscription!: Subscription;

  constructor( private countriesService: CountryService) {
  }

  ngOnInit(): void {
    this.allCountries = this.countriesService.getCountries();
    this.countriesChangeSubscription = this.countriesService.countriesChange.subscribe((countries:Country[]) => {
      this.allCountries = countries;

    });
    this.countriesFetchingSubscription = this.countriesService.countriesFetching.subscribe((isFetching: boolean) => {
      this.loading = isFetching;
    })
    this.countriesService.fetchCountries();
  }
  ngOnDestroy(){
    this.countriesFetchingSubscription.unsubscribe();
    this.countriesChangeSubscription.unsubscribe();
  }


}
