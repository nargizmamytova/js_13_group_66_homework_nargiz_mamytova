import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AllCountriesComponent } from './all-countries/all-countries.component';
import { CountriesDetailsComponent } from './countries-details/countries-details.component';
import { CountryService } from './shared/country.service';
import { HttpClientModule } from '@angular/common/http';
import { NotFounds } from './not-founds';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    AllCountriesComponent,
    CountriesDetailsComponent,
    NotFounds
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [CountryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
